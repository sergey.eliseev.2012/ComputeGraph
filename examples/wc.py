import CG
import re


def get_words(text):
    return re.compile('\w+').findall(text)


def split_text(rec):
    result = []
    for word in get_words(rec['text']):
        result += [{'w': word}]
    return result


def count_word(recs):
    return {recs[0]['w']: len(recs)}


word_count_compute_graph = CG.ComputeGraphBuilder() \
    .read("resources/text_corpus.txt") \
    .map(split_text) \
    .reduce(('w',), count_word) \
    .write("examples/wc_output_new.txt") \
    .build()

word_count_compute_graph.run()

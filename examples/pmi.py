import CG
import math
import re
import logging

logging.basicConfig(filename='pmi.log', level=logging.WARN)


def get_words(text):
    return re.compile('\w+').findall(text)


def count_recs(old_value, r):
    return {'docs_count': old_value['docs_count'] + 1}


def split_text(rec):
    result = []
    for word in get_words(rec['text']):
        result += [{'doc_id': rec['doc_id'], 'word': word}]
    return result


def word_count_per_doc(records):
    return {'word': records[0]['word'], 'doc_id': records[0]['doc_id'], 'doc_freq': len(records)}


def word_freq_reducer(records):
    return {'word': records[0]['word'], 'all_doc_freq': len(records)}


def in_every_doc_and_long_enough(records):
    word = records[0]['word']
    valid = (records[0]['docs_count'] == len(records)) and len(word) > 3
    return {'word': word, 'valid': valid}


def filter_not_valid(r):
    return [{'word': r['word']}] if r['valid'] else []


def count_pmi(r):
    word_freq_in_doc = r['doc_freq']
    word_freq_in_all_docs = r['all_doc_freq']
    return [{'word': r['word'], 'pmi': math.log(word_freq_in_doc / word_freq_in_all_docs), 'doc_id': r['doc_id']}]


def get_keys(keys, record):
    return tuple([record[key] for key in keys])


def sort(keys, records, reverse):
    return sorted(records, key=lambda sorted_rec: get_keys(keys, sorted_rec), reverse=reverse)


def top_ten_by_pmi(records):
    sorted_by_pmi = sort(('pmi',), records, True)[0:min(len(records), 10)]
    top_pmi = []
    for r in sorted_by_pmi:
        top_pmi += [{'word': r['word'], 'pmi': r['pmi']}]
    return {'doc_id': records[0]['doc_id'], 'top_ten': top_pmi}


def word_count_per_doc_more_than_2(record):
    return [record] if record['doc_freq'] > 1 else []


text = CG.ComputeGraphBuilder() \
    .read("../resources/text_corpus.txt") \
    .build()

doc_words = CG.ComputeGraphBuilder() \
    .input(text) \
    .map(split_text) \
    .build()

word_freq_per_doc = CG.ComputeGraphBuilder() \
    .input(doc_words) \
    .reduce(('doc_id', 'word'), word_count_per_doc) \
    .build()

doc_count = CG.ComputeGraphBuilder() \
    .input(text) \
    .fold(count_recs, {'docs_count': 0}) \
    .build()

word_freq = CG.ComputeGraphBuilder() \
    .input(doc_words) \
    .reduce(('word',), word_freq_reducer) \
    .build()

word_freq_per_doc_plus_doc_count = CG.ComputeGraphBuilder() \
    .input(word_freq_per_doc) \
    .join(doc_count, (), CG.JOIN.INNER) \
    .build()

valid_words = CG.ComputeGraphBuilder() \
    .input(word_freq_per_doc_plus_doc_count) \
    .map(word_count_per_doc_more_than_2) \
    .reduce(('word',), in_every_doc_and_long_enough) \
    .map(filter_not_valid) \
    .build()

CG.ComputeGraphBuilder() \
    .input(valid_words) \
    .join(word_freq_per_doc, ('word',), CG.JOIN.INNER) \
    .join(word_freq, ('word',), CG.JOIN.INNER) \
    .map(count_pmi) \
    .reduce(('doc_id',), top_ten_by_pmi) \
    .write('../examples/pmi_new.txt') \
    .build() \
    .run()

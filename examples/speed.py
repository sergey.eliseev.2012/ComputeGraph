import logging
from datetime import datetime

import CG
from geopy.distance import vincenty
from dateutil import tz

logging.basicConfig(filename='speed.log', level=logging.WARN)

# lat long
def distance_meters(first_point, second_point):
    return vincenty(first_point, second_point).meters


def add_meters(record):
    return [{'edge_id': record['edge_id'], 'distance': distance_meters(record['start'][::-1], record['end'][::-1])}]


def parseTime(str):
    zoned_date_time = None
    try:
        zoned_date_time = datetime.strptime(str, '%Y%m%dT%H%M%S.%f')
    except ValueError:
        zoned_date_time = datetime.strptime(str, '%Y%m%dT%H%M%S')

    utc_zone = tz.tzutc()
    moscow_zone = tz.gettz('Europe/Moscow')
    zoned_date_time = zoned_date_time.replace(tzinfo=utc_zone)
    zoned_date_time = zoned_date_time.astimezone(moscow_zone)
    return zoned_date_time


def add_day_and_hour(record):
    enter_zoned_date_time = parseTime(record['enter_time'])
    leave_zoned_date_time = parseTime(record['leave_time'])
    time_sec = (leave_zoned_date_time - enter_zoned_date_time).total_seconds()
    return [{'edge_id': record['edge_id'], 'time': time_sec, 'day': enter_zoned_date_time.strftime('%a'),
             'hour': str(enter_zoned_date_time.time().hour)}]


def count_speed(records):
    distance = 0
    time = 0
    for r in records:
        distance += r['distance']
        time += r['time']
    speed_meters_per_sec = None
    speed_km_per_hour = None
    if time != 0:
        speed_meters_per_sec = (distance / time)
        speed_km_per_hour = speed_meters_per_sec * 3.6
    return {'day': records[0]['day'], 'hour': records[0]['hour'], 'distance': distance, 'time': time,
            'm_per_sec': speed_meters_per_sec, '_km_per_hour': speed_km_per_hour}


milage = CG.ComputeGraphBuilder() \
    .read("../resources/graph_data.txt") \
    .map(add_meters) \
    .build()

times = CG.ComputeGraphBuilder() \
    .read("../resources/travel_times.txt") \
    .map(add_day_and_hour) \
    .build()

speed = CG.ComputeGraphBuilder() \
    .input(milage) \
    .join(times, ('edge_id',), CG.JOIN.INNER) \
    .reduce(('day', 'hour'), count_speed) \
    .write('../examples/speed.txt')\
    .build()\
    .run()

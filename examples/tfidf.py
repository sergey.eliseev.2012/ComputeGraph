import CG
import math
import re


def get_words(text):
    return re.compile('\w+').findall(text)


def split_text(rec):
    result = []
    for word in get_words(rec['text']):
        result += [{'doc_id': rec['doc_id'], 'word': word}]
    return result


def count_recs(old_value, r):
    return {'docs_count': old_value['docs_count'] + 1}


def unique(records):
    return records[0]


def idf(records):
    docs_count = records[0]['docs_count']
    docs_with_word = len(records)
    word = records[0]['word']
    idf = math.log(docs_count / docs_with_word)
    return {'word': word, 'idf': idf}


def tf(records):
    return {'word': records[0]['word'], 'doc_id': records[0]['doc_id'], 'tf': len(records)}


def count_tf_idf(r):
    new_rec = {'doc_id': r['doc_id'], 'word': r['word'], 'tfidf': r['tf'] * r['idf']}
    return [new_rec]


def get_keys(keys, record):
    return tuple([record[key] for key in keys])


def sort(keys, records, reverse):
    return sorted(records, key=lambda sorted_rec: get_keys(keys, sorted_rec), reverse=reverse)


def prepare_answer(records):
    word_docs = []
    for r in records:
        word_docs.append({'tfidf': r['tfidf'], 'doc_id': r['doc_id']})
    top_docs = sort(('tfidf',), word_docs, True)[0:4]
    return {'word': records[0]['word'], 'top_docs': top_docs}


text_read = CG.ComputeGraphBuilder() \
    .read("../resources/text_corpus.txt") \
    .build()

split_word = CG.ComputeGraphBuilder() \
    .input(text_read) \
    .map(split_text) \
    .build()

tfg = CG.ComputeGraphBuilder() \
    .input(split_word) \
    .reduce(('doc_id', 'word'), tf) \
    .build()

count_docs = CG.ComputeGraphBuilder() \
    .input(text_read) \
    .fold(count_recs, {'docs_count': 0}) \
    .build()

count_idf = CG.ComputeGraphBuilder() \
    .input(split_word) \
    .reduce(('doc_id', 'word'), unique) \
    .join(count_docs, (), CG.JOIN.INNER) \
    .reduce(('word',), idf) \
    .join(tfg, ('word',), CG.JOIN.INNER) \
    .map(count_tf_idf) \
    .reduce(('word',), prepare_answer) \
    .write("../examples/tfidf.txt") \
    .build()

count_idf.run()

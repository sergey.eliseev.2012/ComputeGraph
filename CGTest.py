import pytest
import CG


def test_map_one_to_one():
    stage = CG.MapComputeGraphStage(lambda x: [x])
    map_input = ['1']
    map_output = list(stage.run(map_input))
    assert map_input == map_output


def test_map_one_to_empty():
    stage = CG.MapComputeGraphStage(lambda x: [])
    map_input = ['1', '2']
    map_output = list(stage.run(map_input))
    assert len(map_output) == 0


def test_map_one_to_many():
    stage = CG.MapComputeGraphStage(lambda x: [x, x])
    map_input = ['1']
    map_output = list(stage.run(map_input))
    assert map_input * 2 == map_output


def test_sort_one_key():
    sort_input = [{'key': 3}, {'key': 2}]
    stage = CG.SortComputeGraphStage(('key',))
    sort_ouput = list(stage.run(sort_input))
    assert sort_ouput == [{'key': 2}, {'key': 3}]


def test_sort_multiple_keys():
    key_name1 = 'key1'
    key_name2 = 'key2'
    sort_input = [{key_name1: 1, key_name2: 2}, {key_name1: 1, key_name2: 1}]
    stage = CG.SortComputeGraphStage((key_name1, key_name2))
    sort_ouput = list(stage.run(sort_input))
    assert sort_ouput == [{key_name1: 1, key_name2: 1}, {key_name1: 1, key_name2: 2}]


def test_fold():
    stage = CG.FoldComputeGraphStage(lambda curr_value, record: curr_value + record, 0)
    fold_input = [1, 2, 3]
    fold_output = list(stage.run(fold_input))
    assert fold_output == [6]


def test_fold_no_records_returns_initial_value():
    init_value = 3
    stage = CG.FoldComputeGraphStage(lambda curr_value, record: curr_value + record, init_value)
    fold_input = []
    fold_output = list(stage.run(fold_input))
    assert fold_output == [init_value]


def test_inner_join_match():
    left_input = [{"a": "a", "b": "b"}]
    right_input = [{"a": "a", "c": "c"}]
    join_output = CG.JOIN.INNER.join(('a',), left_input, right_input)
    assert join_output == [{"a": "a", "b": "b", "c": "c"}]


def test_inner_join_empty_keys():
    left_input = [{"a": "a", "b": "b"}]
    right_input = [{"a": "a", "c": "c"}]
    join_output = CG.JOIN.INNER.join((), left_input, right_input)
    assert join_output == [{"a": "a", "b": "b", "c": "c"}]


def test_inner_join_empty_keys_multiple():
    left_input = [{"a": "a", "b": "b"}, {"a": "a", "b": "b"}]
    right_input = [{"a": "a", "c": "c"}, {"a": "a", "c": "c"}]
    join_output = CG.JOIN.INNER.join((), left_input, right_input)
    assert len(join_output) == 4


def test_inner_join_no_match():
    left_input = [{"a": "a", "b": "b"}]
    right_input = [{"a": "b", "c": "c"}]
    join_output = CG.JOIN.INNER.join(('a',), left_input, right_input)
    assert join_output == []


def test_inner_join_multiple_match():
    left_input = [{"a": "a", "b": "b"}, {"a": "a", "b": "b"}]
    right_input = [{"a": "a", "c": "c"}, {"a": "a", "c": "c"}]
    join_output = CG.JOIN.INNER.join(('a',), left_input, right_input)
    assert len(join_output) == 4


def test_left_join():
    left_input = [{"a": "a", "b": "b"}, {"a": "b", "b": "b"}]
    right_input = [{"a": "b", "c": "c"}]
    join_output = CG.JOIN.LEFT.join(('a',), left_input, right_input)
    assert join_output == [{"a": "a", "b": "b"}, {"a": "b", "b": "b", "c": "c"}]


def test_left_join_no_right():
    left_input = [{"a": "a", "b": "b"}, {"a": "b", "b": "b"}]
    right_input = []
    join_output = CG.JOIN.LEFT.join(('a',), left_input, right_input)
    assert join_output == left_input


def test_right_join_no_left():
    left_input = []
    right_input = [{"a": "a", "b": "b"}, {"a": "b", "b": "b"}]
    join_output = CG.JOIN.RIGHT.join(('a',), left_input, right_input)
    assert join_output == right_input


def test_right_join():
    left_input = [{"a": "b", "c": "c"}]
    right_input = [{"a": "a", "b": "b"}, {"a": "b", "b": "b"}]
    join_output = CG.JOIN.RIGHT.join(('a',), left_input, right_input)
    assert join_output == [{"a": "a", "b": "b"}, {"a": "b", "b": "b", "c": "c"}]


def test_outer_join_no_match():
    left_input = [{"a": "b", "c": "c"}]
    right_input = [{"a": "a", "b": "b"}]
    join_output = CG.JOIN.OUTER.join(('a',), left_input, right_input)
    assert join_output == [{"a": "b", "c": "c"}, {"a": "a", "b": "b"}]


def test_outer_join_match():
    left_input = [{"a": "b", "c": "c"}]
    right_input = [{"a": "b", "b": "b"}]
    join_output = CG.JOIN.OUTER.join(('a',), left_input, right_input)
    assert join_output == [{"a": "b", "c": "c", "b": "b"}]


def test_outer_join_match_and_no_match():
    left_input = [{"a": "b", "c": "c"}, {"a": "e", "c": "c"}]
    right_input = [{"a": "b", "b": "b"}, {"a": "g", "b": "b"}]
    join_output = CG.JOIN.OUTER.join(('a',), left_input, right_input)
    assert join_output == [{"a": "b", "c": "c", "b": "b"}, {"a": "e", "c": "c"}, {"a": "g", "b": "b"}]

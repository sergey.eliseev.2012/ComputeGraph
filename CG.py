import json
from enum import Enum
import logging


class GraphState(Enum):
    NEW = 1
    IN_PROGRESS = 2
    DONE = 3


class ComputeGraph(object):
    def __init__(self, stages):
        self.__stages = stages
        self.__state = GraphState.NEW
        self.__cached_result = []
        self.__expected_calls = 1
        self.__used_as_dependency = False


    def increase_expected_calls(self):
        if(self.__used_as_dependency):
            self.__expected_calls += 1
        self.__used_as_dependency = True

    def run(self):
        """perform all stages"""
        self.__expected_calls -= 1
        if self.__state == GraphState.DONE:
            if self.__expected_calls < 0:
                raise ComputeGraphException("Graph already computed")
            result = self.__cached_result
            if self.__expected_calls == 0:
                self.__cached_result = None
            return result

        if self.__state == GraphState.IN_PROGRESS:
            raise ComputeGraphException("Cycle dependency detected.")

        self.__state = GraphState.IN_PROGRESS

        curr_stage_result = []
        for stage in self.__stages:
            curr_stage_result = list(stage.run(curr_stage_result))
            logging.debug("Stage finished")
            for r in curr_stage_result:
                logging.debug("{}".format(r))
        if self.__expected_calls > 0:
            self.__cached_result = curr_stage_result
        self.__state = GraphState.DONE
        return curr_stage_result


class ComputeGraphBuilder(object):
    def __init__(self):
        self.__stages = []

    def read(self, file_name):
        """Read an input file lines. Each line should be json object."""
        self.__stages += [ReadComputeGraphStage(file_name)]
        return self

    def input(self, graph):
        """Takes another graph as input"""
        graph.increase_expected_calls()
        self.__stages += [GraphComputeGraphStage(graph)]
        return self

    def map(self, map_func):
        """Map operation should get a record and return a list of records"""
        self.__stages += [MapComputeGraphStage(map_func)]
        return self

    def reduce(self, keys, reduce_func):
        """Reduce list of records to one records after grouping by keys"""
        self.__stages += [ReduceComputeGraphStage(keys, reduce_func)]
        return self

    def fold(self, fold_operation, initial_value):
        """Fold list of records to one record one by one, starting with initial_value"""
        self.__stages += [FoldComputeGraphStage(fold_operation, initial_value)]
        return self

    def write(self, file_name):
        """Write records to ouput file. Each record in new line in json format"""
        self.__stages += [WriteComputeGraphStage(file_name)]
        return self

    def join(self, join_with, on, join_strategy):
        """Join records with another graph according to join strategy"""
        join_with.increase_expected_calls()
        self.__stages += [JoinComputeGraphStage(join_with, on, join_strategy)]
        return self

    def build(self):
        return ComputeGraph(self.__stages)


class ComputeGraphStage(object):
    def __init__(self):
        pass

    def run(self, records):
        """return list of records"""
        raise NotImplemented()


class JOIN(Enum):
    OUTER = 1
    INNER = 2
    LEFT = 3
    RIGHT = 4

    def join(self, keys, left, right):
        if self == JOIN.OUTER:
            return self.__outer_join(keys, left, right)
        elif self == JOIN.INNER:
            return self.__inner_join(keys, left, right)
        elif self == JOIN.LEFT:
            return self.__left_join(keys, left, right)
        elif self == JOIN.RIGHT:
            return self.__left_join(keys, right, left)

    def __get_keys(self, keys, record):
        return tuple([record[key] for key in keys])

    def __sort(self, keys, records):
        return sorted(records, key=lambda sorted_rec: self.__get_keys(keys, sorted_rec))

    def __group_by(self, keys, records):
        groups = {}
        for r in records:
            key = self.__get_keys(keys, r)
            if key not in groups:
                groups[key] = []
            groups[key] += [r]
        return groups

    def __cartesian_product(self, left_records, right_records):
        result = []
        for lr in left_records:
            for rl in right_records:
                result += [{**lr, **rl}]
        return result

    def __inner_join(self, keys, left_records, right_records):
        left_groups = self.__group_by(keys, left_records)
        right_groups = self.__group_by(keys, right_records)
        join_result = []
        for key, left_group_records in left_groups.items():
            if key not in right_groups:
                continue
            right_group_records = right_groups[key]
            join_result += self.__cartesian_product(left_group_records, right_group_records)
        return join_result

    def __left_join(self, keys, left_records, right_records):
        left_groups = self.__group_by(keys, left_records)
        right_groups = self.__group_by(keys, right_records)
        join_result = []
        for key, left_group_records in left_groups.items():
            right_group_records = [{}] if key not in right_groups else right_groups[key]
            join_result += self.__cartesian_product(left_group_records, right_group_records)
        return join_result

    def __outer_join(self, keys, left_records, right_records):
        left_groups = self.__group_by(keys, left_records)
        right_groups = self.__group_by(keys, right_records)
        join_result = []
        for key, left_group_records in left_groups.items():
            right_group_records = [{}] if key not in right_groups else right_groups[key]
            join_result += self.__cartesian_product(left_group_records, right_group_records)
        for key, right_group_records in right_groups.items():
            if key in left_groups:
                continue
            join_result += self.__cartesian_product([{}], right_group_records)
        return join_result


class JoinComputeGraphStage(ComputeGraphStage):
    def __init__(self, join_with, on, join_strategy):
        super().__init__()
        self.__join_with = join_with
        self.__on = on
        self.__join_strategy = join_strategy

    def run(self, records):
        logging.info("Start JoinComputeGraphStage")
        right = self.__join_with.run()
        yield from self.__join_strategy.join(self.__on, records, right)


class MapComputeGraphStage(ComputeGraphStage):
    def __init__(self, map_operation):
        """map_operation is lambda that takes dict and return list of dicts"""
        super().__init__()
        self.__map_operation = map_operation

    def run(self, records):
        logging.info("Start MapComputeGraphStage")
        for r in records:
            yield from self.__map_operation(r)


class FoldComputeGraphStage(ComputeGraphStage):
    def __init__(self, fold_operation, initial_value):
        super().__init__()
        self.__fold_operation = fold_operation
        self.__initial_value = initial_value

    def run(self, records):
        logging.info("Start FoldComputeGraphStage")
        curr_value = self.__initial_value
        for r in records:
            curr_value = self.__fold_operation(curr_value, r)
        yield curr_value


class ReduceComputeGraphStage(ComputeGraphStage):
    def __init__(self, reduce_keys, reduce_operation):
        super().__init__()
        self.__reduce_operation = reduce_operation
        if len(reduce_keys) == 0:
            raise ComputeGraphException("Provide reduce keys, or use map instead")
        self.__group_by_keys = reduce_keys

    def __get_keys(self, record):
        try:
            return tuple([record[key] for key in self.__group_by_keys])
        except:
            raise KeyError(record)

    def __sort(self, records):
        return sorted(records, key=lambda sorted_rec: self.__get_keys(sorted_rec))

    def __group_by(self, sorted_records):
        if len(sorted_records) == 0:
            return []
        group_by_map = {}
        first_record = sorted_records[0]
        first_group_key = self.__get_keys(first_record)
        group_by_map[first_group_key] = []
        group_by_map[first_group_key] += [first_record]
        for r in sorted_records[1:]:
            key = self.__get_keys(r)
            if key not in group_by_map:
                group_by_map[key] = []
            group_by_map[key] += [r]
        return group_by_map.values()

    def run(self, records):
        logging.info("Start ReduceComputeGraphStage")
        sorded_records = self.__sort(records)
        grouped_by_lists = self.__group_by(sorded_records)
        for grouped_by_list in grouped_by_lists:
            yield self.__reduce_operation(grouped_by_list)


class SortComputeGraphStage(ComputeGraphStage):
    def __init__(self, sort_keys):
        super().__init__()
        if len(sort_keys) == 0:
            raise ComputeGraphException("Provide sort keys")
        self.__sort_keys = sort_keys

    def __get_keys(self, record):
        return tuple([record[key] for key in self.__sort_keys])

    def run(self, records):
        logging.info("Start SortComputeGraphStage")
        yield from sorted(records, key=lambda sorted_rec: self.__get_keys(sorted_rec))


class GraphComputeGraphStage(ComputeGraphStage):
    def __init__(self, graph):
        super().__init__()
        self.__graph = graph

    def run(self, records):
        logging.info("Input GraphComputeGraphState")
        yield from self.__graph.run()


class ReadComputeGraphStage(ComputeGraphStage):
    def __init__(self, input_file):
        super().__init__()
        self.__input_file = input_file

    def run(self, records):
        logging.info("Start ReadComputeGraphStage {}".format(self.__input_file))
        with open(self.__input_file, "r") as file:
            for line in file.readlines():
                yield json.loads(line)


class WriteComputeGraphStage(ComputeGraphStage):
    def __init__(self, output_file):
        super().__init__()
        self.__output_file = output_file

    def run(self, records):
        logging.info("Start WriteComputeGraphStage {}".format(self.__output_file))
        with open(self.__output_file, 'w') as f:
            for r in records:
                json.dump(r, f, ensure_ascii=False)
                f.write('\n')

        yield {'file': self.__output_file}


class ComputeGraphException(Exception):
    def __init__(self, message):
        super(Exception, self).__init__(message)
